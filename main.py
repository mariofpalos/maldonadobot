#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pandas as pd
import random

sustantivosRaw  = pd.read_csv('data/sustantivos.csv')

#print(sustantivos.values.tolist())

sustantivos = []

for row in sustantivosRaw.values:
    for item in row:
        if str(item) != "nan":
            sustantivos.append(item)

answers = random.sample(sustantivos, random.randint(1,3))
#add gaussian prob.
if random.randint(1,20) == 1:
    answers.append("Te quiero en mi equipo.")

for item in answers:
    print(item)
#print(sustantivos)
